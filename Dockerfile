FROM php:7.4-apache

ARG TIMEZONE
ARG PUID=1000
ARG PGID=1000

ENV PUID ${PUID}
ENV PGID ${PGID}
ENV APACHE_RUN_USER docker
ENV APACHE_RUN_GROUP docker

# install dependencies
RUN apt-get update -yqq \
    && apt-get install -y git zip unzip libzip-dev libcap2-bin \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/

RUN pecl install xdebug-2.8.1 \
    && docker-php-ext-enable xdebug \
    && docker-php-ext-install pdo_mysql \
    && docker-php-ext-install zip \
    && docker-php-ext-install ctype \
    && docker-php-ext-install tokenizer

# create user in order to prevent permission issues
RUN  groupadd --gid $PGID $APACHE_RUN_GROUP && \
     useradd --home /home/$APACHE_RUN_USER --uid $PUID --gid $PGID $APACHE_RUN_USER && \
     usermod -a -G www-data $APACHE_RUN_USER && \
     mkdir -p /home/$APACHE_RUN_USER/.composer && \
     chown -R $APACHE_RUN_USER:$APACHE_RUN_GROUP /home/$APACHE_RUN_USER

# set vhost
ADD ./vhost.conf /etc/apache2/sites-enabled/000-default.conf

# set timezone
RUN ln -snf /usr/share/zoneinfo/${TIMEZONE} /etc/localtime && echo ${TIMEZONE} > /etc/timezone
RUN printf '[PHP]\ndate.timezone = "%s"\n', ${TIMEZONE} > /usr/local/etc/php/conf.d/tzone.ini

# install composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php -r "if (hash_file('sha384', 'composer-setup.php') === 'e0012edf3e80b6978849f5eff0d4b4e4c79ff1609dd1e613307e16318854d24ae64f26d17af3ef0bf7cfb710ca74755a') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php');"
RUN mv composer.phar /usr/local/bin/composer
RUN chown $APACHE_RUN_USER:$APACHE_RUN_GROUP /usr/local/bin/composer

# configure apache
RUN a2enmod rewrite
RUN setcap 'cap_net_bind_service=+ep' /usr/sbin/apache2

# configure PHP
RUN cp /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini
RUN echo 'memory_limit = -1' >> /usr/local/etc/php/php.ini

RUN mkdir -p /var/log/apache2
RUN chown -R $APACHE_RUN_USER:$APACHE_RUN_GROUP /var/log/apache2
RUN chmod -R 777 /var/log/apache2

# setup workdir
WORKDIR /project

# switch to non-root user
USER $APACHE_RUN_USER

# install Laravel installer
RUN composer global require laravel/installer
RUN echo 'export PATH="$HOME/.composer/vendor/bin:$PATH"' >> $HOME/.bashrc

EXPOSE 80 443
